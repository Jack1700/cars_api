import express, {Router} from 'express';
import {ajv} from "../validation/validation";
import CarController from "../controllers/car";
import { ICar } from '../models/car';


const carRouter: Router = express.Router();


// Basic CRUD routes for rules
carRouter.get("/car", async (req, res) => {
  const controller = new CarController();
  const cars = await controller.getCars().catch(err => {
    res.status(500).send(err.message);
  });
  return res.send(cars);
});

carRouter.get("/car/:id", async (req, res) => {
  const controller = new CarController();
  const car = await controller.getCar(req.params.id).catch(err => {
    res.status(500).send(err.message);
  });
  return res.send(car);
});

carRouter.post("/car", async (req, res) => {
  const controller = new CarController();
  const validate = ajv.getSchema<ICar>("car")!
  if (validate(req.body)) {
    // create car
    const car = await controller.createCar(req.body).catch(err => {
      res.status(500).send(err.message);
    });
    return res.send(car);
  } else {
    // report error
    res.status(400).send(validate.errors);
  }
});

carRouter.put("/car/:id", async (req, res) => {
  const controller = new CarController();
  const validate = ajv.getSchema<ICar>("car")!
  if (validate(req.body)) {
    // update car
    const car = await controller.updateCar(req.params.id, req.body).catch(err => {
      res.status(500).send(err.message);
    });
    return res.send(car);
  } else {
    // report error
    res.status(400).send(validate.errors);
  }
});

carRouter.delete("/car/:id", async (req, res) => {
  const controller = new CarController();
  const car = await controller.deleteCar(req.params.id).catch(err => {
    res.status(500).send(err.message);
  });
  return res.send(car);
});

export default carRouter;