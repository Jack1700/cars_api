import express, {Router} from 'express';
import carsRouter from './car';


const router: Router = express.Router();

// PROTECT ALL ROUTES THAT FOLLOW
router.use((req, res, next) => {
  const apiKey = req.get('x-api-key')
  if (!apiKey || apiKey !== process.env.API_KEY) {
    res.status(401).json({error: 'unauthorised'})
  } else {
    next()
  }
})

// Forward user routes to the user router
router.use(carsRouter);


export default router;
