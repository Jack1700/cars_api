import { model, Schema, Model, Document } from 'mongoose';

export interface ICar extends Document {
  name: string;
  model: string;
  year: number;
  price: number;
  description: string;
}

const carSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    year: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: false
    },
});


export const Car: Model<ICar> = model('Car', carSchema);


