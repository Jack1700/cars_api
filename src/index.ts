import express, {Application} from 'express';
import Router from './routes';

const mongoose = require('mongoose');
const url = process.env.MONGODB_URI || 'mongodb://localhost:27017/cars';

const PORT = process.env.PORT || 8080;

// Connect to MongoDB
mongoose
  .connect(
    url,
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch((err: any) => console.log(err));

const app: Application = express();

app.use(express.json());

app.use(Router);

app.listen(PORT, () => {
  console.log('Server is running on port', PORT);
});

