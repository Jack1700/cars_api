import { Car, ICar } from "../models/car";

// Rule Controller
export default class CarController {
  public async getCar(id: string): Promise<ICar> {
    const car = await Car.findById(id);
    // check if car exists
    if (!car) {
      throw new Error("Car not found");
    } else {
      return car;
    }
  }

  public async getCars(): Promise<ICar[]> {
    const cars = await Car.find();
    // check if car exists
    if (!cars) {
      throw new Error("Cars not found");
    } else {
      return cars;
    }
  }

  public async createCar(car: ICar): Promise<ICar> {
    const newCar = new Car(car);
    return newCar.save();
  }

  public async updateCar(id: string, car: ICar): Promise<ICar> {
    const updatedCar = await Car.findByIdAndUpdate(id, car, { new: true });
    if (!updatedCar) {
      throw new Error("Car not found");
    } else {
      return updatedCar;
    }
  }

  public async deleteCar(id: string): Promise<ICar> {
    const deletedCar = await Car.findByIdAndDelete(id);
    if (!deletedCar) {
      throw new Error("Car not found");
    } else {
      return deletedCar;
    }
  }
}
