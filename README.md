<div id="top"></div>


<br />
<div align="center">
  <h3 align="center">Cars API</h3>

  <p align="center">
    A simple car api with CRUD operations!
  </p>
</div>

## Getting Started

This is a simple backend for creating cars using CRUDE operations. 
All incoming requests are validated with ajv before being stored in the database. 
In the docker compose file you can set various envierment variables, like an api key or which mongo backend is used. 
By default, docker starts its own mongodb and uses it as persistent storage.
The default API Key is abc123 and should be set in the header under the name x-api-key.


### Installation

1. Install Docker
2. Open a terminal inside the Cars folder
3. Run "docker build -t cars_app ."
4. Run "docker compose up"


### Built With

* [TypeScript](https://www.typescriptlang.org/)
* [NodeJs](https://nodejs.org/)
* [Express](https://expressjs.com/)
* [Mongoose](https://mongoosejs.com/)
* [Docker](https://www.docker.com/)
* [AJV](https://ajv.js.org/)




<p align="right">(<a href="#top">back to top</a>)</p>
